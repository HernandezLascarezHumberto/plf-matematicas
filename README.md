# PROGRAMACION LOGICA Y FUNCIONAL ISB

## MATEMATICAS

### 1.- CONJUNTOS, APLICACIONES Y FUNCIONES (2002).

```plantuml
@startmindmap
*[#08F79C] Conjuntos, Aplicaciones y funciones
**[#3DF5CB] Conjunto
***[#6DD7B8] Ejemplos:
****[#6DD7B8] A = {1,2,3}, B = {1,{1}, {2,3}}. \n N = {1,2,3,...} el conjuto de los números naturales.\n Z = {..., -2,-1,0,1,2,3,...} el conjunto de l,os numeros enteros. \n Q = {a/b; a ∈ z, b ∈ N} el conjunto de los números racionales. \n R: es el conjunto de los numeros reales, C el conjunto de numeros complejos. \n ∅ o {} el conjunto vacio.
***[#6DD7B8] Se le puede
****[#6DD7B8] Llamar producto cartesiano del conjunto por el conjunto 
****[#6DD7B8] Simbolicamente:
*****[#A4E8D4] A x B = {(x,y)/x ∈ A ∧ y ∈ B}
***[#6DD7B8] Es una 
****[#6DD7B8] Coleccion de objetos, llamados \n elementos, que tienen la propiedad que dado un objeto cualquiera
****[#6DD7B8] el Orden de los elementos no importa en un conjunto, \n y en un conjunto no se tiene en cuentarepeticiones de elementos.
*****[#A4E8D4] Se dice que cada elemento a de un conjunto A pertenece al conjunto A
******[#A4E8D4] Se denota a ∈ A
****[#6DD7B8] Si un Objeto b no pertenece al conjunto A se nota b /∈ A.
*****[#A4E8D4] Ejemplos:
******[#A4E8D4] Sea A = {1,2,3}: 1 ∈ A, 2 ∈ A, 4 ∈/ A, {1,2} ∈/ A. \n Sea B = {2, {1},{2,3}}: {1} ∈ B, {2,3} ∈ B, 1 ∈/ B, 3 ∈/ B
***[#6DD7B8] Operaciones entre conjuntos
****[#6DD7B8] Complemento 
*****[#A4E8D4] Sea A subconjunto de un conjunto referencial U.\n El complemento de A (en U) es el conjunto A´ de los elementos de U que no pertenecen a A. 
******[#A4E8D4] Es decir 
*******[#A4E8D4] A´ = {b ∈ U : b /∈ A}
*******[#A4E8D4] ∀ b ∈ U, b ∈ A´ ⇔ b /∈ A
********[#A4E8D4] Si U = {1,2,3} y A = entonces A´ = {1,3}. \n Si U = N y A, entonces A´ = {n ∈ N, n/= 2}.
****[#6DD7B8] Unión
*****[#A4E8D4] Sea A, B subconjuntos de un conjunto referencial U.\n La union de A y B es el conjunto A U B de los elementos de U que pertenecen a A o a B
******[#A4E8D4] Es decir A U B = {C ∈ U: C ∈ A y C ∈ B} \n ∀ C ∈ U, C ∈ A U ⇔ C ∈ A o C ∈ B
*******[#A4E8D4] Ejemplos: 
********[#A4E8D4] Si A ={1,2,3,5,8} y B={3,4,5,10} ⊆ U ={1,...,10}, entonces \n A U B ={1,2,3,4,5,8,10}. \n Si I = {x ∈ R: x ≤ 2} = (-∞, 2]y J={x ∈ R: -10 ≤ x &lt; 10} = [-10, 10) ⊆ U = R, entonces I U J = {x ∈ R: x &lt;10} = (-∞, 10)
****[#6DD7B8] Interseccion
*****[#A4E8D4] Sean A, B subconjuntos de un conjunto referencial U.\n La interseccion de A y B es el conjunto A ∩ B de los elementos de U que pertenecen tanto a A como a B
******[#A4E8D4] Es decir \n A ∩ B={C ∈ U : C ∈ A y C ∈ B}\n C ∈ A ∩ B ⇔ C ∈ A y C ∈ B
*******[#A4E8D4] Ejemplos:
********[#A4E8D4] Sean A = {1,2,3,5,8}, B = {3,4,5,10}⊆ U ={1,...,10}, entonces \n A ∩ B= {3,5}\n Sean I = {x ∈ R: x ≤ 2} = (-∞, 2]y J={x ∈ R: -10 ≤ x <10} = [-10, 10) ⊆ U = R. Entonces I ∩ J = {x ∈ R: -10 ≤ x ≤2}= [-10, 2]
**[#3DF5CB] Diferentes definiciones de conjunto
***[#6DD7B8] Extencion o enumeracion
****[#6DD7B8] Sus elementos son encerrados entre3 llaves
*****[#A4E8D4] Separados por comas, cada conjunto describe un listado de todos los elementos 
***[#6DD7B8] Comprension 
****[#6DD7B8] Sus elementos se determinan a traves de una condicion que se establece entre llaves
***[#6DD7B8] Diagramas de Venn
****[#6DD7B8] Regiones cerradas que nos permiten visualizar las relaciones entre los conjuntos 
***[#6DD7B8] Descrtipcion verbal
****[#6DD7B8] Enunciado que describe una caracteristica comun a todos los elementos del conjunto
**[#3DF5CB] Funciones
***[#6DD7B8] Tipos de funciones 
****[#6DD7B8] Inyectiva
*****[#A4E8D4] La imagen de un determinado x ∈ A\n es uno y solo un elemento de y ∈ B
*****[#A4E8D4] f:A ==> es inyectiva si ∀a,a´ ∈, f(a) = f(a´) ==> a = a´
****[#6DD7B8] Sobreyectiva
*****[#A4E8D4] Se dice que una funcion es sobreyectiva si \n para todo b de B existe un a de A tal que f(a)=b
******[#A4E8D4] Es decir si todo el elemento del conjunto de salida A tiene su correspondiente imagen en B
****[#6DD7B8] Biyectiva
*****[#A4E8D4] Funcion biyectiva o biunivoca si es inyectiva y sobreyectiva  
***[#6DD7B8] El conjunto de salida es igual al conjunto de definicion (A = dominio de f)
@endmindmap
```
### 2.- FUNCIONES (2010)
```plantuml
@startmindmap
*[#08F79C] FUNCION 
**[#3DF5CB] Funcion matematica
***[#6DD7B8] Es una relacion que se establece entre dos conjuntos, \n a traves de la cual a cada elemento del primer \n conjunto se le asigna un unico elemento del segundo conjunto o ninguno
****[#6DD7B8] A conjunto inicial o conjunto de partida tambien  \n se lo llama dominio 
*****[#6DD7B8] Como tal, se puede aplicar a diversas situaciones,\n tanto en la vida cotidiana como en las ciencias, donde se advierten relaciones\n de dependencia entre dos elementos
***[#6DD7B8] Nos sirven para modelar diversas relaciones entre distintos \n fenomenos o situaciones
****[#6DD7B8] Se refiere a la relacion de correspondencia existente\n entre dos conjuntos, donde cada elemento del primer conjunto se encuentra\n relacionado con uno del segundo
**[#3DF5CB] Un funcion f es una relacion entre un conjunto dado X (el dominio)\n y otro conjunto de elementos Y (el codominio)
***[#6DD7B8] De forma que cada elemento\n x del dominio le corresponde un unico elemento del codominio f(x)
****[#6DD7B8] se denota \n f: X ->Y
*****[#6DD7B8] Una primera idea de funcion es la de una formula que relaciona \n algebraicamente varias magnitudes
******[#6DD7B8] Una reprecentacion algebraica
*******[#6DD7B8] Mediante diagramas cartecianos permite la visualizacion de las funciones 
*******[#6DD7B8] Concepto de funcion se generaliza a cualquier relacion numerica que responda a una grafica sobre unos ejes coordenados
**[#3DF5CB] Las funciones describen fenomenos cotidianos economicos,psicologicos, cientificos
***[#6DD7B8] Se obtiene experimentalmente mediante la observacion.
***[#6DD7B8] Las funciones definidas a trozos, las cuales \n requieren de varias formas de las cuales rige el comportamiento\n de la funcion en un cierto tramo
****[#6DD7B8] Familia de funciones 
*****[#6DD7B8] Lineales
******[#6DD7B8] f(x)=a*x+b;
*****[#6DD7B8] Cuadraticas 
******[#6DD7B8] f(x)=a*xa^2+b*x+c;
*****[#6DD7B8] Funcion raiz
******[#6DD7B8] f(x)= sqrt(k*x);
*****[#6DD7B8] Funciones de proporcionalidad inversa
******[#6DD7B8] f(x)=k/x;
*****[#6DD7B8] Funciones exponenciales
******[#6DD7B8] f(x)=a^x;
*****[#6DD7B8] Funciones logaritmicas
******[#6DD7B8] f(x)= log(x);
*****[#6DD7B8] Funciones trigonometricas 
******[#6DD7B8] f(x)= sin(x);\nf(x)=cos(x);\nf(x)=tan(x);
**[#3DF5CB] La manera habitual de denotar una funcion f
***[#6DD7B8] Es \n f.A -> B\n a -> f(a) 
****[#6DD7B8] Donde A es el dominio de la funcion f\n es el primenr conjunto o conjunto partida
****[#6DD7B8] Donde B es el dominio de f \n es el segundo conjunto o conjunto de llegada
****[#6DD7B8] Denota la regla o algoritmo para obtener la imagen de un cierto objeto arbitrario a del dominio A
@endmindmap
```
### 3.- MATEMATICA DEL COMPUTADOR (2002)
```plantuml
@startmindmap
*[#08F79C] La matematica del computador 
**[#3DF5CB] Aritmetica de la computadora
***[#6DD7B8] El ususrio que se comunica con la computadora en sistema decimal
****[#6DD7B8] Introduce an ella y extrae de ella numeros en base decimal
*****[#A4E8D4] Recibe datos 
******[#A4E8D4] Para trabajar con ellos
******[#A4E8D4] La computadora los convierte al sistema binario
******[#A4E8D4] Su lenguaje normal de operacion
*******[#A4E8D4] 1010101110010101010101
***[#6DD7B8] Operaciones
****[#6DD7B8] Resultados obtenidos
*****[#A4E8D4] La computadora realiza estos procesos a enormes velocidades
****[#6DD7B8] Todas la operaciones se efectuan en binarios
*****[#A4E8D4] La maquina los conviete al sistema decimal
***[#6DD7B8] La operacion interna de una computadora se basa en la aritmetica binaria 
****[#6DD7B8] La base es el 2 y solo hay dos simbolos: 0 y 1 
*****[#A4E8D4] La memoria de la maquina consiste en un vasto\n numero de dispositivos de registro magnetico y electronico
**[#3DF5CB] Matematica computacional
***[#6DD7B8] Es una herramienta basica para un ingeniero o cientifico
****[#6DD7B8] Soluciona de forma aproximada, \n problemas practicas que podrian \n resolverse de manera analitica
***[#6DD7B8] Se dice que la matematica computacional es la matematica mas fundamental que existe
****[#6DD7B8] Para resolver problemas hacen uso de las operaciones aritmeticas. 
*****[#A4E8D4] Esquema
******[#A4E8D4] Problema real
*******[#A4E8D4] Modelo matematico
********[#A4E8D4] Resolucion
*********[#A4E8D4] Tematica Tradicional\n (Metodos analiticos)\n(Solucion exacta)
**********[#A4E8D4] Resultados
*********[#A4E8D4] Matematica computacional\n (Metodos numericos)\n(Solucion Aproximada)
**********[#A4E8D4] Resultados
***[#6DD7B8] Ejemplo:
****[#6DD7B8] Verificacion de la corriente en un circuito electronico
*****[#A4E8D4] Ecuacion Diferencial
******[#A4E8D4] Resolucion
*******[#A4E8D4] Variables separadas\n Transformada de Lplace, ect.
********[#A4E8D4] Resultado
*******[#A4E8D4] Metodo de Euler.\nMetodo de Runge-kuta, etc.
********[#A4E8D4] Resultado
***[#6DD7B8] Numeros de maquinas
****[#6DD7B8] Utiluza un computador como apoyo a la matematica computacional
*****[#A4E8D4] Fundamental considerar las peculiaridades del sistema numerico del computador 
******[#A4E8D4] Numeros Aritmeticos
*******[#A4E8D4] No tienen Principio ni fin\nCada numero esta representado por un opuesto\n Son infinitos
******[#A4E8D4] Numeros de maquina
*******[#A4E8D4] Tine principio y fin\n Cada numero representa un subintervalo\n Son finitos
@endmindmap
```